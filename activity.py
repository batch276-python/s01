name = "Leizl"
age = 36
occupation = "business owner"
movie = "Ant-Man and the Wasp: Quantumania"
rating = 97.5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%.")


num1 = 3
num2 = 9
num3 = 16

# a

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)
